

# Tx


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique transaction identifier |  [optional]
**slip44** | **Integer** | SLIP-44 coin ID |  [optional]
**addresses** | **Set&lt;String&gt;** | List of involved addresses (excluding contracts) |  [optional]
**assets** | **Set&lt;String&gt;** | List of moved assets by asset path |  [optional]
**date** | **Long** | Unix timestamp |  [optional]
**height** | **Long** | Number of block if mined, otherwise omitted. |  [optional]
**blockId** | **String** | ID of block if mined, otherwise omitted. |  [optional]
**status** | [**StatusEnum**](#StatusEnum) | Result status of the transaction. |  [optional]
**tags** | **Set&lt;String&gt;** | List of tags for this transaction |  [optional]
**operations** | [**Map&lt;String, Operation&gt;**](Operation.md) | Operations in this transaction (opaque keys). |  [optional]
**effects** | [**Map&lt;String, Effect&gt;**](Effect.md) | Effects by address, if supported |  [optional]



## Enum: StatusEnum

Name | Value
---- | -----
COMPLETED | &quot;completed&quot;
FAILED | &quot;failed&quot;



