package com.gitlab.blockdaemon.ubiquity.tx.btc.exceptions;

public class ScriptInvalidException extends RuntimeException {
    public ScriptInvalidException() {
    }

    @SuppressWarnings("unused")
    public ScriptInvalidException(String s) {
        super(s);
    }
}
