

# AlgorandMeta


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** |  |  [optional]
**senderReward** | **String** |  |  [optional]
**recipientReward** | **String** |  |  [optional]
**close** | **String** |  |  [optional]
**closeAmount** | **String** |  |  [optional]
**closeReward** | **String** |  |  [optional]



